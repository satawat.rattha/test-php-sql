<?php

function db()
{
    $host = '127.0.0.1';
    $dbname = 'test';
    $user = 'root';
    $pass = 'root';

    return new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
}
