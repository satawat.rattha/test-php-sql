<?php

require_once('database.php');

// ป้องกัน sql injection โดยใช้การ binding data แทนการ นำไปใส่ใน sql ตรงๆ

function search($search)
{
    $sql = 'SELECT tb_product.name as productName,tb_product.id AS productId,tb_shop.id as shopId ,tb_shop.name as shopName,tb_product.status
    FROM tb_product 
    INNER JOIN tb_shop ON tb_product.shop_id = tb_shop.id
    WHERE tb_shop.name LIKE :shop_name';

    $sth = db()->prepare($sql);

    $sth->execute([
        ':shop_name' => "$search"
    ]);

    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

$users = search($_GET['search'] ?? 'rudy shop')

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

    <title>Product</title>
</head>

<body>
    <div class="container mt-4">
        <form class="form">
            <div class="input-group">
                <input type="text" class="form-control" name="search" value="<?= $_GET['search'] ?? 'rudy shop' ?>" placeholder="Shop name">
                <div class="input-group-btn">
                    <button class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <th>#</th>
                <th>Product</th>
                <th>Shop</th>
                <th>Status</th>
            </thead>
            <tbody>
                <?php foreach ($users as $key => $user) : ?>
                    <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $user['productName'] ?></td>
                        <td><?= $user['shopName'] ?></td>
                        <td><?= $user['status'] ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <a href="/" class="btn btn-primary">Back</a>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>
    -->
</body>

</html>