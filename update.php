<?php
require_once('database.php');

function updateProduct($shopName)
{
    $sql = 'UPDATE tb_product SET tb_product.status = 0 WHERE shop_id = (SELECT tb_shop.id FROM tb_shop WHERE tb_shop.name = :shopName LIMIT 1)';

    $sth = db()->prepare($sql);

    return $sth->execute([
        ':shopName' => $shopName
    ]);
}

$result = updateProduct('rudy shop');

if ($result) {
    header('Location: /product.php');
}

echo '<h1>Cannot Update status</h1>';
